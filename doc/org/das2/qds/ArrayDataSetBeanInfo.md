# org.das2.qds.ArrayDataSetBeanInfo
ArrayDataSetBeanInfo( )


***
<a name="getPropertyDescriptors"></a>
# getPropertyDescriptors
getPropertyDescriptors(  ) &rarr; PropertyDescriptor



### Returns:
java.beans.PropertyDescriptor[]


<a href="https://github.com/autoplot/dev/search?q=getPropertyDescriptors&unscoped_q=getPropertyDescriptors">[search for examples]</a>

