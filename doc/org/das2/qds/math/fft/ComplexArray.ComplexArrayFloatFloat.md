# org.das2.qds.math.fft.ComplexArray.ComplexArrayFloatFloatImplements ComplexArray that is backed by two float arrays.
***
<a name="getImag"></a>
# getImag
getImag( int i ) &rarr; float



### Parameters:
i - a int

### Returns:
float


<a href="https://github.com/autoplot/dev/search?q=getImag&unscoped_q=getImag">[search for examples]</a>

***
<a name="getReal"></a>
# getReal
getReal( int i ) &rarr; float



### Parameters:
i - a int

### Returns:
float


<a href="https://github.com/autoplot/dev/search?q=getReal&unscoped_q=getReal">[search for examples]</a>

***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

***
<a name="setImag"></a>
# setImag
setImag( int i, float value ) &rarr; void



### Parameters:
i - a int
<br>value - a float

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setImag&unscoped_q=setImag">[search for examples]</a>

***
<a name="setReal"></a>
# setReal
setReal( int i, float value ) &rarr; void



### Parameters:
i - a int
<br>value - a float

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setReal&unscoped_q=setReal">[search for examples]</a>

