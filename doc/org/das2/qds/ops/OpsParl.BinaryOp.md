# org.das2.qds.ops.OpsParl.BinaryOpBinaryOps are operations such as add, pow, atan2
***
<a name="op"></a>
# op
op( double d1, double d2 ) &rarr; double



### Parameters:
d1 - a double
<br>d2 - a double

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=op&unscoped_q=op">[search for examples]</a>

