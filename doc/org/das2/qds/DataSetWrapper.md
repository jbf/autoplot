# org.das2.qds.DataSetWrapperWraps a dataset, making the properties mutable.  This is also intended to be base class for extension.
DataSetWrapper( QDataSet ds )


***
<a name="length"></a>
# length
length( int i, int j, int k ) &rarr; int



### Parameters:
i - a int
<br>j - a int
<br>k - a int

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

length( int i, int j ) &rarr; int<br>
length( int i ) &rarr; int<br>
length(  ) &rarr; int<br>
***
<a name="property"></a>
# property
property( String name, int i ) &rarr; Object



### Parameters:
name - a String
<br>i - a int

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=property&unscoped_q=property">[search for examples]</a>

property( String name ) &rarr; Object<br>
***
<a name="rank"></a>
# rank
rank(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rank&unscoped_q=rank">[search for examples]</a>

***
<a name="value"></a>
# value
value( int i0, int i1, int i2, int i3 ) &rarr; double



### Parameters:
i0 - a int
<br>i1 - a int
<br>i2 - a int
<br>i3 - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

value( int i0, int i1, int i2 ) &rarr; double<br>
value( int i0, int i1 ) &rarr; double<br>
value( int i ) &rarr; double<br>
