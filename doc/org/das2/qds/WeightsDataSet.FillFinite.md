# org.das2.qds.WeightsDataSet.FillFinitereturn 1 for finite (Non-NaN) values that are not equal to fill, or (float)fill.
FillFinite( QDataSet ds )


***
<a name="value"></a>
# value
value(  ) &rarr; double



### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

value( int i ) &rarr; double<br>
value( int i0, int i1 ) &rarr; double<br>
value( int i0, int i1, int i2 ) &rarr; double<br>
value( int i0, int i1, int i2, int i3 ) &rarr; double<br>
