# org.das2.qds.examples.DemoFunction1First example function just returns sine of the function.
DemoFunction1( )


***
<a name="exampleInput"></a>
# exampleInput
exampleInput(  ) &rarr; QDataSet



### Returns:
org.das2.qds.QDataSet


<a href="https://github.com/autoplot/dev/search?q=exampleInput&unscoped_q=exampleInput">[search for examples]</a>

***
<a name="value"></a>
# value
value( QDataSet parm ) &rarr; QDataSet



### Parameters:
parm - a QDataSet

### Returns:
org.das2.qds.QDataSet


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

