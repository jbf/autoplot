# org.das2.qds.demos.PlasmaModelModel of plasma distribution function for given density, temperature, speed.
 A java.util.Random object is passed in so that the data may be reproducible
 (by using a given starting seed).
PlasmaModel( )


***
<a name="getRank2"></a>
# getRank2
getRank2(  ) &rarr; QDataSet

return a rank 2 dataset with time as DEPEND_0 and energy as DEPEND_1.

### Returns:
a QDataSet


<a href="https://github.com/autoplot/dev/search?q=getRank2&unscoped_q=getRank2">[search for examples]</a>

