# org.das2.qds.demos.RipplesDataSetold das2 ripples dataset, but supports rank 1 and 2.
 See Ops.ripples.
RipplesDataSet( )


RipplesDataSet( int len0 )


RipplesDataSet( int len0, int len1 )


RipplesDataSet( double x1, double y1, double p1, double x2, double y2, double p2, int xlength, int ylength )


***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

length( int i ) &rarr; int<br>
***
<a name="rank"></a>
# rank
rank(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rank&unscoped_q=rank">[search for examples]</a>

***
<a name="value"></a>
# value
value( int i ) &rarr; double



### Parameters:
i - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

value( int i0, int i1 ) &rarr; double<br>
