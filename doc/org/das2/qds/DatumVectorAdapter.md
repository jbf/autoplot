# org.das2.qds.DatumVectorAdapterutility routines for adapting legacy das2 DatumVector.
DatumVectorAdapter( )


***
<a name="toDatumVector"></a>
# toDatumVector
toDatumVector( QDataSet ds ) &rarr; DatumVector

extracts a das2 legacy DatumVector from a rank 1 QDataSet.

### Parameters:
ds - a QDataSet

### Returns:
org.das2.datum.DatumVector


<a href="https://github.com/autoplot/dev/search?q=toDatumVector&unscoped_q=toDatumVector">[search for examples]</a>

