# org.das2.qds.filters.DbAboveBackgroundDim1FilterEditorPanelController for dbAboveBackgroundFilter.
DbAboveBackgroundDim1FilterEditorPanel( )
Creates new form dbAboveBackgroundDim1FilterEditorPanel

***
<a name="buttonGroup1"></a>
# buttonGroup1



***
<a name="jLabel1"></a>
# jLabel1



***
<a name="jLabel2"></a>
# jLabel2



***
<a name="percentTF"></a>
# percentTF



***
<a name="powerRadioButton"></a>
# powerRadioButton



***
<a name="voltageButton"></a>
# voltageButton



***
<a name="getFilter"></a>
# getFilter
getFilter(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=getFilter&unscoped_q=getFilter">[search for examples]</a>

***
<a name="setFilter"></a>
# setFilter
setFilter( String filter ) &rarr; void



### Parameters:
filter - a String

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setFilter&unscoped_q=setFilter">[search for examples]</a>

