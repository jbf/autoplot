# org.das2.qds.filters.PutPropertyFilterEditorPanelEditor panel for the putProperty filter, which allows valid
 ranges and missing metadata to be specified.
PutPropertyFilterEditorPanel( )
Creates new form PutPropertyFilterEditorPanel

***
<a name="getFilter"></a>
# getFilter
getFilter(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=getFilter&unscoped_q=getFilter">[search for examples]</a>

***
<a name="setFilter"></a>
# setFilter
setFilter( String filter ) &rarr; void



### Parameters:
filter - a String

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setFilter&unscoped_q=setFilter">[search for examples]</a>

