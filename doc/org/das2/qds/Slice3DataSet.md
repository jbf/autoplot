# org.das2.qds.Slice3DataSetreturn a rank N-1 dataset from a rank N dataset by slicing on the fourth
 dimension.  (Rank 4 supported.)
 
 plane datasets are sliced as well, when they have rank 4 or greater.
Slice3DataSet( QDataSet ds, int index )


***
<a name="equals"></a>
# equals
equals( Object obj ) &rarr; boolean



### Parameters:
obj - a Object

### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=equals&unscoped_q=equals">[search for examples]</a>

***
<a name="hashCode"></a>
# hashCode
hashCode(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=hashCode&unscoped_q=hashCode">[search for examples]</a>

***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

length( int i ) &rarr; int<br>
length( int i0, int i1 ) &rarr; int<br>
***
<a name="property"></a>
# property
property( String name ) &rarr; Object



### Parameters:
name - a String

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=property&unscoped_q=property">[search for examples]</a>

***
<a name="rank"></a>
# rank
rank(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rank&unscoped_q=rank">[search for examples]</a>

***
<a name="value"></a>
# value
value( int i0, int i1, int i2 ) &rarr; double



### Parameters:
i0 - a int
<br>i1 - a int
<br>i2 - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

