# org.das2.qds.util.AsciiHeadersParser.BundleDescriptor
***
<a name="indexOf"></a>
# indexOf
indexOf( String name ) &rarr; int



### Parameters:
name - a String

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=indexOf&unscoped_q=indexOf">[search for examples]</a>

***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

length( int i ) &rarr; int<br>
***
<a name="property"></a>
# property
property( String name, int ic ) &rarr; Object



### Parameters:
name - a String
<br>ic - a int

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=property&unscoped_q=property">[search for examples]</a>

***
<a name="putProperty"></a>
# putProperty
putProperty( String name, int ic, Object v ) &rarr; void



### Parameters:
name - a String
<br>ic - a int
<br>v - a Object

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=putProperty&unscoped_q=putProperty">[search for examples]</a>

***
<a name="rank"></a>
# rank
rank(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rank&unscoped_q=rank">[search for examples]</a>

***
<a name="trim"></a>
# trim
trim( int start, int end ) &rarr; QDataSet

special code because of START_INDEX property.  Must trim at DataSet boundaries.

### Parameters:
start - a int
<br>end - a int

### Returns:
a QDataSet


<a href="https://github.com/autoplot/dev/search?q=trim&unscoped_q=trim">[search for examples]</a>

***
<a name="value"></a>
# value
value( int i0, int i1 ) &rarr; double



### Parameters:
i0 - a int
<br>i1 - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

