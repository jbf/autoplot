# org.das2.qds.util.BundleBuilderreduce and standardize code needed to build bundle descriptors used in BUNDLE_1.  This can
 only build a bundle of rank 1 datasets.
BundleBuilder( int size )
create the builder for the BundleDescriptor to go into BUNDLE_1 property.

***
<a name="getDataSet"></a>
# getDataSet
getDataSet(  ) &rarr; QDataSet



### Returns:
org.das2.qds.QDataSet


<a href="https://github.com/autoplot/dev/search?q=getDataSet&unscoped_q=getDataSet">[search for examples]</a>

***
<a name="putProperty"></a>
# putProperty
putProperty( String name, int index, Object value ) &rarr; void



### Parameters:
name - a String
<br>index - a int
<br>value - a Object

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=putProperty&unscoped_q=putProperty">[search for examples]</a>

