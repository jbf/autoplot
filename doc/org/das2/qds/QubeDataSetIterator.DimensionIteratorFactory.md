# org.das2.qds.QubeDataSetIterator.DimensionIteratorFactoryDimensionIteratorFactory creates DimensionIterators
***
<a name="newIterator"></a>
# newIterator
newIterator( int len ) &rarr; DimensionIterator



### Parameters:
len - a int

### Returns:
org.das2.qds.QubeDataSetIterator.DimensionIterator


<a href="https://github.com/autoplot/dev/search?q=newIterator&unscoped_q=newIterator">[search for examples]</a>

