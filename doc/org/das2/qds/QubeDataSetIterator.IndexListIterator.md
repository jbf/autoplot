# org.das2.qds.QubeDataSetIterator.IndexListIteratorIterator that goes through a list of indeces.
IndexListIterator( QDataSet ds )


***
<a name="hasNext"></a>
# hasNext
hasNext(  ) &rarr; boolean



### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=hasNext&unscoped_q=hasNext">[search for examples]</a>

***
<a name="index"></a>
# index
index(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=index&unscoped_q=index">[search for examples]</a>

***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

***
<a name="nextIndex"></a>
# nextIndex
nextIndex(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=nextIndex&unscoped_q=nextIndex">[search for examples]</a>

***
<a name="toString"></a>
# toString
toString(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=toString&unscoped_q=toString">[search for examples]</a>

