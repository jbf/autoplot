# org.das2.qds.QubeDataSetIterator.SingletonIteratorFactoryFactory for iterator for a single index.
SingletonIteratorFactory( int index )


***
<a name="newIterator"></a>
# newIterator
newIterator( int length ) &rarr; DimensionIterator



### Parameters:
length - a int

### Returns:
org.das2.qds.QubeDataSetIterator.DimensionIterator


<a href="https://github.com/autoplot/dev/search?q=newIterator&unscoped_q=newIterator">[search for examples]</a>

