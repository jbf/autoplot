# org.das2.util.filesystem.FtpFileSystemFactory
FtpFileSystemFactory( )
Creates a new instance of FtpFileSystemFactory

***
<a name="createFileSystem"></a>
# createFileSystem
createFileSystem( java.net.URI root ) &rarr; FileSystem



### Parameters:
root - a URI

### Returns:
org.das2.util.filesystem.FileSystem


<a href="https://github.com/autoplot/dev/search?q=createFileSystem&unscoped_q=createFileSystem">[search for examples]</a>

