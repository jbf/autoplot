# org.das2.util.filesystem.HttpFileSystemFactoryCreates a FileSystem for reading files via HTTP and HTTPS.
HttpFileSystemFactory( )


***
<a name="createFileSystem"></a>
# createFileSystem
createFileSystem( java.net.URI root ) &rarr; FileSystem



### Parameters:
root - a URI

### Returns:
org.das2.util.filesystem.FileSystem


<a href="https://github.com/autoplot/dev/search?q=createFileSystem&unscoped_q=createFileSystem">[search for examples]</a>

