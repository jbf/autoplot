# org.das2.util.filesystem.FileSystem.FileSystemOfflineExceptionException indicating the file system is off-line.  For example, if the network is not
 available, fresh listings of an http site cannot be accessed.
FileSystemOfflineException( )


FileSystemOfflineException( String message )


FileSystemOfflineException( java.io.IOException e )


FileSystemOfflineException( java.io.IOException e, java.net.URI root )


