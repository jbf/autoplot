# org.das2.util.NBConsoleFormatterFormats log messages to the stdout, so that Netbeans will automatically
 make links from the stack traces printed to stderr.
NBConsoleFormatter( )


***
<a name="format"></a>
# format
format( java.util.logging.LogRecord rec ) &rarr; String



### Parameters:
rec - a LogRecord

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=format&unscoped_q=format">[search for examples]</a>

