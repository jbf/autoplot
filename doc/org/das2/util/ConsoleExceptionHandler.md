# org.das2.util.ConsoleExceptionHandlerExceptionHandler that prints stack traces out to the stderr.
ConsoleExceptionHandler( )
Creates a new instance of ConsoleExceptionHandler

***
<a name="handle"></a>
# handle
handle( java.lang.Throwable t ) &rarr; void



### Parameters:
t - a Throwable

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=handle&unscoped_q=handle">[search for examples]</a>

***
<a name="handleUncaught"></a>
# handleUncaught
handleUncaught( java.lang.Throwable t ) &rarr; void



### Parameters:
t - a Throwable

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=handleUncaught&unscoped_q=handleUncaught">[search for examples]</a>

