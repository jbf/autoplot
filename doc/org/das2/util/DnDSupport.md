# org.das2.util.DnDSupport
***
<a name="setParent"></a>
# setParent
setParent( org.das2.util.DnDSupport parent ) &rarr; void



### Parameters:
parent - a DnDSupport

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setParent&unscoped_q=setParent">[search for examples]</a>

***
<a name="startDrag"></a>
# startDrag
startDrag( int x, int y, int action, java.awt.event.MouseEvent evt ) &rarr; void



### Parameters:
x - a int
<br>y - a int
<br>action - a int
<br>evt - a MouseEvent

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=startDrag&unscoped_q=startDrag">[search for examples]</a>

