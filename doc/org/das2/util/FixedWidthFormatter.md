# org.das2.util.FixedWidthFormatter
FixedWidthFormatter( )


***
<a name="format"></a>
# format
format( String s, int nchars ) &rarr; String



### Parameters:
s - a String
<br>nchars - a int

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=format&unscoped_q=format">[search for examples]</a>

***
<a name="formatWide"></a>
# formatWide
formatWide( String s, int nchars ) &rarr; String



### Parameters:
s - a String
<br>nchars - a int

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=formatWide&unscoped_q=formatWide">[search for examples]</a>

