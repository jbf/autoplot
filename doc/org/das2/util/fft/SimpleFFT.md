# org.das2.util.fft.SimpleFFT
***
<a name="fft"></a>
# fft
fft( double[][] array ) &rarr; double



### Parameters:
array - a double[][]

### Returns:
double[][]


<a href="https://github.com/autoplot/dev/search?q=fft&unscoped_q=fft">[search for examples]</a>

