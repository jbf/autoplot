# org.das2.math.Interpolate.FDoubleArray
***
<a name="columns"></a>
# columns
columns(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=columns&unscoped_q=columns">[search for examples]</a>

***
<a name="get"></a>
# get
get( int i, int j ) &rarr; float



### Parameters:
i - a int
<br>j - a int

### Returns:
float


<a href="https://github.com/autoplot/dev/search?q=get&unscoped_q=get">[search for examples]</a>

***
<a name="put"></a>
# put
put( int i, int j, float value ) &rarr; void



### Parameters:
i - a int
<br>j - a int
<br>value - a float

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=put&unscoped_q=put">[search for examples]</a>

***
<a name="rows"></a>
# rows
rows(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rows&unscoped_q=rows">[search for examples]</a>

