# org.das2.math.matrix.Matrix
***
<a name="columnCount"></a>
# columnCount
columnCount(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=columnCount&unscoped_q=columnCount">[search for examples]</a>

***
<a name="copy"></a>
# copy
copy( org.das2.math.matrix.Matrix m ) &rarr; void



### Parameters:
m - a Matrix

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=copy&unscoped_q=copy">[search for examples]</a>

***
<a name="get"></a>
# get
get( int row, int col ) &rarr; double



### Parameters:
row - a int
<br>col - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=get&unscoped_q=get">[search for examples]</a>

***
<a name="rowCount"></a>
# rowCount
rowCount(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rowCount&unscoped_q=rowCount">[search for examples]</a>

***
<a name="rowTimes"></a>
# rowTimes
rowTimes( int row, double s ) &rarr; void



### Parameters:
row - a int
<br>s - a double

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=rowTimes&unscoped_q=rowTimes">[search for examples]</a>

***
<a name="rowTimesAddTo"></a>
# rowTimesAddTo
rowTimesAddTo( int srcRow, double s, int dstRow ) &rarr; void



### Parameters:
srcRow - a int
<br>s - a double
<br>dstRow - a int

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=rowTimesAddTo&unscoped_q=rowTimesAddTo">[search for examples]</a>

***
<a name="set"></a>
# set
set( int row, int col, double d ) &rarr; void



### Parameters:
row - a int
<br>col - a int
<br>d - a double

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=set&unscoped_q=set">[search for examples]</a>

***
<a name="swapRows"></a>
# swapRows
swapRows( int row1, int row2 ) &rarr; void



### Parameters:
row1 - a int
<br>row2 - a int

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=swapRows&unscoped_q=swapRows">[search for examples]</a>

***
<a name="toString"></a>
# toString
toString(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=toString&unscoped_q=toString">[search for examples]</a>

