# org.das2.system.NullMonitorFactoryMonitorFactory implementation that always returns a null monitor.
NullMonitorFactory( )


***
<a name="getMonitor"></a>
# getMonitor
getMonitor( org.das2.graph.DasCanvasComponent context, String label, String description ) &rarr; ProgressMonitor



### Parameters:
context - a DasCanvasComponent
<br>label - a String
<br>description - a String

### Returns:
org.das2.util.monitor.ProgressMonitor


<a href="https://github.com/autoplot/dev/search?q=getMonitor&unscoped_q=getMonitor">[search for examples]</a>

getMonitor( String label, String description ) &rarr; ProgressMonitor<br>
getMonitor( org.das2.graph.DasCanvas canvas, String string, String string0 ) &rarr; ProgressMonitor<br>
