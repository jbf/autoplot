# org.das2.system.MutatorLocksee DasAxis.getMutatorLock, DasDevicePosition.getMutatorLock
 TODO: This needs work, because there is no way to query if the lock was
 successful.
***
<a name="lock"></a>
# lock
lock(  ) &rarr; void



### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=lock&unscoped_q=lock">[search for examples]</a>

***
<a name="unlock"></a>
# unlock
unlock(  ) &rarr; void



### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=unlock&unscoped_q=unlock">[search for examples]</a>

