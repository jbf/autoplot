# org.das2.DasProperties.Editor
***
<a name="actionPerformed"></a>
# actionPerformed
actionPerformed( java.awt.event.ActionEvent e ) &rarr; void



### Parameters:
e - a ActionEvent

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=actionPerformed&unscoped_q=actionPerformed">[search for examples]</a>

***
<a name="setDirty"></a>
# setDirty
setDirty( boolean dirty ) &rarr; void



### Parameters:
dirty - a boolean

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setDirty&unscoped_q=setDirty">[search for examples]</a>

