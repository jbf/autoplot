# org.das2.stream.StripHeader
StripHeader( )


***
<a name="main"></a>
# main
main( java.lang.String[] args ) &rarr; void



### Parameters:
args - a java.lang.String[]

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=main&unscoped_q=main">[search for examples]</a>

***
<a name="stripHeader"></a>
# stripHeader
stripHeader( java.io.InputStream in, java.io.OutputStream out ) &rarr; void



### Parameters:
in - a InputStream
<br>out - a OutputStream

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=stripHeader&unscoped_q=stripHeader">[search for examples]</a>

