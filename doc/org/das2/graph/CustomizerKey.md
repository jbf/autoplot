# org.das2.graph.CustomizerKey
***
<a name="compareTo"></a>
# compareTo
compareTo( org.das2.graph.CustomizerKey other ) &rarr; int



### Parameters:
other - a CustomizerKey

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=compareTo&unscoped_q=compareTo">[search for examples]</a>

***
<a name="equals"></a>
# equals
equals( Object obj ) &rarr; boolean



### Parameters:
obj - a Object

### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=equals&unscoped_q=equals">[search for examples]</a>

***
<a name="hashCode"></a>
# hashCode
hashCode(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=hashCode&unscoped_q=hashCode">[search for examples]</a>

***
<a name="of"></a>
# of
of( String label ) &rarr; CustomizerKey



### Parameters:
label - a String

### Returns:
org.das2.graph.CustomizerKey


<a href="https://github.com/autoplot/dev/search?q=of&unscoped_q=of">[search for examples]</a>

***
<a name="toString"></a>
# toString
toString(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=toString&unscoped_q=toString">[search for examples]</a>

