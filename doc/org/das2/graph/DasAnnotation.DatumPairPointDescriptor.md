# org.das2.graph.DasAnnotation.DatumPairPointDescriptorcreate a PointDescriptor using and x and y Datum.
DatumPairPointDescriptor( org.das2.graph.DasPlot p, Datum x, Datum y )


***
<a name="getLabel"></a>
# getLabel
getLabel(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=getLabel&unscoped_q=getLabel">[search for examples]</a>

***
<a name="getPoint"></a>
# getPoint
getPoint(  ) &rarr; Point



### Returns:
java.awt.Point


<a href="https://github.com/autoplot/dev/search?q=getPoint&unscoped_q=getPoint">[search for examples]</a>

