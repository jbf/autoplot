# org.das2.graph.DasColorBar.ColorBarLayoutManagerTODO: Ed document me
***
<a name="layoutContainer"></a>
# layoutContainer
layoutContainer( java.awt.Container parent ) &rarr; void



### Parameters:
parent - a Container

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=layoutContainer&unscoped_q=layoutContainer">[search for examples]</a>

