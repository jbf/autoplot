# org.das2.graph.DasAnnotation.PointDescriptorsomething to point at
***
<a name="getLabel"></a>
# getLabel
getLabel(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=getLabel&unscoped_q=getLabel">[search for examples]</a>

***
<a name="getPoint"></a>
# getPoint
getPoint(  ) &rarr; Point



### Returns:
java.awt.Point


<a href="https://github.com/autoplot/dev/search?q=getPoint&unscoped_q=getPoint">[search for examples]</a>

