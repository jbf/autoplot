# org.das2.graph.PathIterableA PathIterable is an object that can return a PathIterator for the
***
<a name="pathIterator"></a>
# pathIterator
pathIterator( java.awt.geom.AffineTransform at ) &rarr; PathIterator



### Parameters:
at - a AffineTransform

### Returns:
java.awt.geom.PathIterator


<a href="https://github.com/autoplot/dev/search?q=pathIterator&unscoped_q=pathIterator">[search for examples]</a>

