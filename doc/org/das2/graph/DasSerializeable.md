# org.das2.graph.DasSerializeable
***
<a name="getDOMElement"></a>
# getDOMElement
getDOMElement( org.w3c.dom.Document document ) &rarr; Element



### Parameters:
document - a Document

### Returns:
org.w3c.dom.Element


<a href="https://github.com/autoplot/dev/search?q=getDOMElement&unscoped_q=getDOMElement">[search for examples]</a>

***
<a name="processElement"></a>
# processElement
processElement( org.w3c.dom.Element element, org.das2.graph.DasPlot parent, org.das2.dasml.FormBase form ) &rarr; Object



### Parameters:
element - a Element
<br>parent - a DasPlot
<br>form - a FormBase

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=processElement&unscoped_q=processElement">[search for examples]</a>

