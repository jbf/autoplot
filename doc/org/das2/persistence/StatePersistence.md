# org.das2.persistence.StatePersistenceProvides object serialization, using delegates to handle das2 immutable objects.
***
<a name="restoreState"></a>
# restoreState
restoreState( java.io.InputStream in ) &rarr; Object



### Parameters:
in - a InputStream

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=restoreState&unscoped_q=restoreState">[search for examples]</a>

***
<a name="saveState"></a>
# saveState
saveState( java.io.OutputStream out, Object state ) &rarr; void



### Parameters:
out - a OutputStream
<br>state - a Object

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=saveState&unscoped_q=saveState">[search for examples]</a>

