# org.das2.persistence.UnitsPersistenceDelegate
UnitsPersistenceDelegate( )
Creates a new instance of UnitsPersistenceDelegate

***
<a name="writeObject"></a>
# writeObject
writeObject( Object oldInstance, java.beans.Encoder out ) &rarr; void



### Parameters:
oldInstance - a Object
<br>out - a Encoder

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=writeObject&unscoped_q=writeObject">[search for examples]</a>

