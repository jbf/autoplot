# org.das2.dasml.SerializeUtil
SerializeUtil( )


***
<a name="getDOMElement"></a>
# getDOMElement
getDOMElement( org.w3c.dom.Document document, Object object ) &rarr; Element



### Parameters:
document - a Document
<br>object - a Object

### Returns:
org.w3c.dom.Element


<a href="https://github.com/autoplot/dev/search?q=getDOMElement&unscoped_q=getDOMElement">[search for examples]</a>

getDOMElement( org.w3c.dom.Document document, Object object, ProgressMonitor monitor ) &rarr; Element<br>
***
<a name="processElement"></a>
# processElement
processElement( org.w3c.dom.Element element, Object object ) &rarr; void



### Parameters:
element - a Element
<br>object - a Object

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=processElement&unscoped_q=processElement">[search for examples]</a>

