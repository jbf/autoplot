# org.das2.dasml.OptionList
***
<a name="getOptions"></a>
# getOptions
getOptions(  ) &rarr; ListOption



### Returns:
org.das2.dasml.ListOption[]


<a href="https://github.com/autoplot/dev/search?q=getOptions&unscoped_q=getOptions">[search for examples]</a>

***
<a name="setOptions"></a>
# setOptions
setOptions( org.das2.dasml.ListOption[] options ) &rarr; void



### Parameters:
options - a org.das2.dasml.ListOption[]

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setOptions&unscoped_q=setOptions">[search for examples]</a>

