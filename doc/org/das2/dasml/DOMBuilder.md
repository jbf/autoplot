# org.das2.dasml.DOMBuilderNew stateful serialize utility that uses names when available.
DOMBuilder( Object bean )


***
<a name="serialize"></a>
# serialize
serialize( org.w3c.dom.Document document, ProgressMonitor monitor ) &rarr; Element



### Parameters:
document - a Document
<br>monitor - a ProgressMonitor

### Returns:
org.w3c.dom.Element


<a href="https://github.com/autoplot/dev/search?q=serialize&unscoped_q=serialize">[search for examples]</a>

