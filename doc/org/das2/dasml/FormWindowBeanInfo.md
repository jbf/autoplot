# org.das2.dasml.FormWindowBeanInfo
FormWindowBeanInfo( )


***
<a name="getMethodDescriptors"></a>
# getMethodDescriptors
getMethodDescriptors(  ) &rarr; MethodDescriptor



### Returns:
java.beans.MethodDescriptor[]


<a href="https://github.com/autoplot/dev/search?q=getMethodDescriptors&unscoped_q=getMethodDescriptors">[search for examples]</a>

