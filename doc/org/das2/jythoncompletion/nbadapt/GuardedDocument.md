# org.das2.jythoncompletion.nbadapt.GuardedDocument
GuardedDocument( )


***
<a name="isPosGuarded"></a>
# isPosGuarded
isPosGuarded( int caretOffset ) &rarr; boolean



### Parameters:
caretOffset - a int

### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=isPosGuarded&unscoped_q=isPosGuarded">[search for examples]</a>

