# org.das2.jythoncompletion.ui.CompletionItemComparatorComparator for completion items either by sort priority or by sort text.
***
<a name="BY_PRIORITY"></a>
# BY_PRIORITY



***
<a name="ALPHABETICAL"></a>
# ALPHABETICAL



***
<a name="compare"></a>
# compare
compare( org.das2.jythoncompletion.support.CompletionItem i1, org.das2.jythoncompletion.support.CompletionItem i2 ) &rarr; int



### Parameters:
i1 - a CompletionItem
<br>i2 - a CompletionItem

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=compare&unscoped_q=compare">[search for examples]</a>

***
<a name="get"></a>
# get
get( int sortType ) &rarr; Comparator



### Parameters:
sortType - a int

### Returns:
java.util.Comparator


<a href="https://github.com/autoplot/dev/search?q=get&unscoped_q=get">[search for examples]</a>

