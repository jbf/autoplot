# org.das2.jythoncompletion.JythonCompletionTaskCompletions for Jython code.  The completion task is created with the
 editor configured for completions (code and caret position within code),
 and "query" is called which will fill a CompletionResultSet.
JythonCompletionTask( javax.swing.text.JTextComponent t )
create the completion task on the text component, using its content and caret position.

***
<a name="CLIENT_PROPERTY_INTERPRETER_PROVIDER"></a>
# CLIENT_PROPERTY_INTERPRETER_PROVIDER



***
<a name="cancel"></a>
# cancel
cancel(  ) &rarr; void



### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=cancel&unscoped_q=cancel">[search for examples]</a>

***
<a name="doQuery"></a>
# doQuery
doQuery( org.das2.jythoncompletion.CompletionContext cc, org.das2.jythoncompletion.support.CompletionResultSet resultSet ) &rarr; int

perform the completions query.

### Parameters:
cc - a CompletionContext
<br>resultSet - a CompletionResultSet

### Returns:
the count

<a href="https://github.com/autoplot/dev/search?q=doQuery&unscoped_q=doQuery">[search for examples]</a>

***
<a name="escapeHtml"></a>
# escapeHtml
escapeHtml( String s ) &rarr; String



### Parameters:
s - a String

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=escapeHtml&unscoped_q=escapeHtml">[search for examples]</a>

***
<a name="getIconFor"></a>
# getIconFor
getIconFor( Object jm ) &rarr; ImageIcon

return an identifying icon for the object, or null.

### Parameters:
jm - java.lang.reflect.Method, or PyInteger, etc.

### Returns:
the icon or null.

<a href="https://github.com/autoplot/dev/search?q=getIconFor&unscoped_q=getIconFor">[search for examples]</a>

***
<a name="getImportableCompletions"></a>
# getImportableCompletions
getImportableCompletions( String source, org.das2.jythoncompletion.CompletionContext cc, org.das2.jythoncompletion.support.CompletionResultSet result ) &rarr; int

get completions by looking at importLookup.jy, which is a list of commonly imported codes.

### Parameters:
source - the script source.
<br>cc - a CompletionContext
<br>result - a CompletionResultSet

### Returns:
a int


<a href="https://github.com/autoplot/dev/search?q=getImportableCompletions&unscoped_q=getImportableCompletions">[search for examples]</a>

***
<a name="getLocalsCompletions"></a>
# getLocalsCompletions
getLocalsCompletions( PythonInterpreter interp, org.das2.jythoncompletion.CompletionContext cc, org.das2.jythoncompletion.support.CompletionResultSet rs ) &rarr; int



### Parameters:
interp - a PythonInterpreter
<br>cc - a CompletionContext
<br>rs - a CompletionResultSet

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=getLocalsCompletions&unscoped_q=getLocalsCompletions">[search for examples]</a>

getLocalsCompletions( PythonInterpreter interp, org.das2.jythoncompletion.CompletionContext cc ) &rarr; List<br>
***
<a name="keySort"></a>
# keySort
keySort( java.util.List key, java.util.List[] lists ) &rarr; void

sorts all the lists by the first list.  
 See http://stackoverflow.com/questions/15400514/syncronized-sorting-between-two-arraylists/24688828#24688828
 Note the key list must be repeated for it to be sorted as well!

### Parameters:
key - the list used to sort
<br>lists - the lists to be sorted, often containing the key as well.

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=keySort&unscoped_q=keySort">[search for examples]</a>

***
<a name="query"></a>
# query
query( org.das2.jythoncompletion.support.CompletionResultSet arg0 ) &rarr; void



### Parameters:
arg0 - a CompletionResultSet

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=query&unscoped_q=query">[search for examples]</a>

***
<a name="refresh"></a>
# refresh
refresh( org.das2.jythoncompletion.support.CompletionResultSet arg0 ) &rarr; void



### Parameters:
arg0 - a CompletionResultSet

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=refresh&unscoped_q=refresh">[search for examples]</a>

