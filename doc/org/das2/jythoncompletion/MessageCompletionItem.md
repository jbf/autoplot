# org.das2.jythoncompletion.MessageCompletionItemCompletion that just shows a message.
MessageCompletionItem( String message )


MessageCompletionItem( String message, String documentation )


***
<a name="createDocumentationTask"></a>
# createDocumentationTask
createDocumentationTask(  ) &rarr; CompletionTask



### Returns:
org.das2.jythoncompletion.support.CompletionTask


<a href="https://github.com/autoplot/dev/search?q=createDocumentationTask&unscoped_q=createDocumentationTask">[search for examples]</a>

***
<a name="createToolTipTask"></a>
# createToolTipTask
createToolTipTask(  ) &rarr; CompletionTask



### Returns:
org.das2.jythoncompletion.support.CompletionTask


<a href="https://github.com/autoplot/dev/search?q=createToolTipTask&unscoped_q=createToolTipTask">[search for examples]</a>

***
<a name="defaultAction"></a>
# defaultAction
defaultAction( javax.swing.text.JTextComponent component ) &rarr; void



### Parameters:
component - a JTextComponent

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=defaultAction&unscoped_q=defaultAction">[search for examples]</a>

***
<a name="getInsertPrefix"></a>
# getInsertPrefix
getInsertPrefix(  ) &rarr; CharSequence



### Returns:
java.lang.CharSequence


<a href="https://github.com/autoplot/dev/search?q=getInsertPrefix&unscoped_q=getInsertPrefix">[search for examples]</a>

***
<a name="getPreferredWidth"></a>
# getPreferredWidth
getPreferredWidth( java.awt.Graphics g, java.awt.Font defaultFont ) &rarr; int



### Parameters:
g - a Graphics
<br>defaultFont - a Font

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=getPreferredWidth&unscoped_q=getPreferredWidth">[search for examples]</a>

***
<a name="getSortPriority"></a>
# getSortPriority
getSortPriority(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=getSortPriority&unscoped_q=getSortPriority">[search for examples]</a>

***
<a name="getSortText"></a>
# getSortText
getSortText(  ) &rarr; CharSequence



### Returns:
java.lang.CharSequence


<a href="https://github.com/autoplot/dev/search?q=getSortText&unscoped_q=getSortText">[search for examples]</a>

***
<a name="instantSubstitution"></a>
# instantSubstitution
instantSubstitution( javax.swing.text.JTextComponent component ) &rarr; boolean



### Parameters:
component - a JTextComponent

### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=instantSubstitution&unscoped_q=instantSubstitution">[search for examples]</a>

***
<a name="processKeyEvent"></a>
# processKeyEvent
processKeyEvent( java.awt.event.KeyEvent evt ) &rarr; void



### Parameters:
evt - a KeyEvent

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=processKeyEvent&unscoped_q=processKeyEvent">[search for examples]</a>

***
<a name="render"></a>
# render
render( java.awt.Graphics graphics, java.awt.Font defaultFont, java.awt.Color defaultColor, java.awt.Color backgroundColor, int width, int height, boolean selected ) &rarr; void



### Parameters:
graphics - a Graphics
<br>defaultFont - a Font
<br>defaultColor - a Color
<br>backgroundColor - a Color
<br>width - a int
<br>height - a int
<br>selected - a boolean

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=render&unscoped_q=render">[search for examples]</a>

