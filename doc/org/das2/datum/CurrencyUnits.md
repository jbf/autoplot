# org.das2.datum.CurrencyUnitsparse a few currencies, for demonstration purposes.
CurrencyUnits( String id, String ch, String desc )


***
<a name="parse"></a>
# parse
parse( String s ) &rarr; Datum

remove the symbol and parse as a double.

### Parameters:
s - a String

### Returns:
a Datum


<a href="https://github.com/autoplot/dev/search?q=parse&unscoped_q=parse">[search for examples]</a>

