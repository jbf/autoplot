# org.das2.datum.InconvertibleUnitsExceptionintroduced so that clients can more precisely catch this exception.
InconvertibleUnitsException( Units fromUnits, Units toUnits )
Creates a new instance of InconvertibleUnitsException

