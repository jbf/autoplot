# org.das2.datum.TimeUtil.TimeDigit
***
<a name="divisions"></a>
# divisions
divisions(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=divisions&unscoped_q=divisions">[search for examples]</a>

***
<a name="fromOrdinal"></a>
# fromOrdinal
fromOrdinal( int ordinal ) &rarr; TimeDigit



### Parameters:
ordinal - a int

### Returns:
org.das2.datum.TimeUtil.TimeDigit


<a href="https://github.com/autoplot/dev/search?q=fromOrdinal&unscoped_q=fromOrdinal">[search for examples]</a>

***
<a name="getOrdinal"></a>
# getOrdinal
getOrdinal(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=getOrdinal&unscoped_q=getOrdinal">[search for examples]</a>

***
<a name="toString"></a>
# toString
toString(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=toString&unscoped_q=toString">[search for examples]</a>

