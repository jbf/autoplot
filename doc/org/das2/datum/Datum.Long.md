# org.das2.datum.Datum.Longclass backing Datums with a long, such as with CDF_TT2000.
Long( long value, Units units )


***
<a name="longValue"></a>
# longValue
longValue( Units u ) &rarr; long



### Parameters:
u - a Units

### Returns:
long


<a href="https://github.com/autoplot/dev/search?q=longValue&unscoped_q=longValue">[search for examples]</a>

