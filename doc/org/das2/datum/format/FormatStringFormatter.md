# org.das2.datum.format.FormatStringFormatterThis is based on the C-style format strings introduced in Java 5 that
 we can now use.  Note this should not be used for times.  In the future this
 may be supported.  TODO: See Autoplot's DataSetUtil.toString, which shows
 use with Calendar objects.
FormatStringFormatter( String formatStr, boolean units )
create a new instance based on the Java format string.

***
<a name="format"></a>
# format
format( Datum datum ) &rarr; String



### Parameters:
datum - a Datum

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=format&unscoped_q=format">[search for examples]</a>

format( Datum datum, Units units ) &rarr; String<br>
***
<a name="toString"></a>
# toString
toString(  ) &rarr; String



### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=toString&unscoped_q=toString">[search for examples]</a>

