# org.das2.datum.format.DatumFormatterFactory
***
<a name="defaultFormatter"></a>
# defaultFormatter
defaultFormatter(  ) &rarr; DatumFormatter



### Returns:
org.das2.datum.format.DatumFormatter


<a href="https://github.com/autoplot/dev/search?q=defaultFormatter&unscoped_q=defaultFormatter">[search for examples]</a>

***
<a name="newFormatter"></a>
# newFormatter
newFormatter( String format ) &rarr; DatumFormatter



### Parameters:
format - a String

### Returns:
org.das2.datum.format.DatumFormatter


<a href="https://github.com/autoplot/dev/search?q=newFormatter&unscoped_q=newFormatter">[search for examples]</a>

