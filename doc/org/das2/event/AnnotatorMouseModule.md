# org.das2.event.AnnotatorMouseModuleDraw a box and add an annotation.
AnnotatorMouseModule( org.das2.graph.DasCanvasComponent parent )
Creates a new instance of AnnotatorMouseModule

***
<a name="getCanvas"></a>
# getCanvas
getCanvas(  ) &rarr; DasCanvas



### Returns:
org.das2.graph.DasCanvas


<a href="https://github.com/autoplot/dev/search?q=getCanvas&unscoped_q=getCanvas">[search for examples]</a>

***
<a name="mouseRangeSelected"></a>
# mouseRangeSelected
mouseRangeSelected( org.das2.event.MouseDragEvent e ) &rarr; void



### Parameters:
e - a MouseDragEvent

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=mouseRangeSelected&unscoped_q=mouseRangeSelected">[search for examples]</a>

