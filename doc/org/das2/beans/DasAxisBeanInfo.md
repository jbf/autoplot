# org.das2.beans.DasAxisBeanInfoBeanInfo class for org.das2.graph.DasAxis.
DasAxisBeanInfo( )


***
<a name="getAdditionalBeanInfo"></a>
# getAdditionalBeanInfo
getAdditionalBeanInfo(  ) &rarr; BeanInfo



### Returns:
java.beans.BeanInfo[]


<a href="https://github.com/autoplot/dev/search?q=getAdditionalBeanInfo&unscoped_q=getAdditionalBeanInfo">[search for examples]</a>

