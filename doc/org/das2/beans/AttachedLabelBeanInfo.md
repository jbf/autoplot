# org.das2.beans.AttachedLabelBeanInfoBeanInfo class for DasColorBar
AttachedLabelBeanInfo( )


***
<a name="getAdditionalBeanInfo"></a>
# getAdditionalBeanInfo
getAdditionalBeanInfo(  ) &rarr; BeanInfo



### Returns:
java.beans.BeanInfo[]


<a href="https://github.com/autoplot/dev/search?q=getAdditionalBeanInfo&unscoped_q=getAdditionalBeanInfo">[search for examples]</a>

