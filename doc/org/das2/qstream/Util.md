# org.das2.qstream.UtilUtility classes for formatting and parsing streams.
Util( )


***
<a name="decodeArray"></a>
# decodeArray
decodeArray( String s ) &rarr; int



### Parameters:
s - a String

### Returns:
int[]


<a href="https://github.com/autoplot/dev/search?q=decodeArray&unscoped_q=decodeArray">[search for examples]</a>

***
<a name="encodeArray"></a>
# encodeArray
encodeArray( int[] qube, int off, int len ) &rarr; String



### Parameters:
qube - a int[]
<br>off - a int
<br>len - a int

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=encodeArray&unscoped_q=encodeArray">[search for examples]</a>

***
<a name="singletonChildElement"></a>
# singletonChildElement
singletonChildElement( org.w3c.dom.Element n ) &rarr; Element

returns the single child element of a node, or throws IllegalArgumentException

### Parameters:
n - a Element

### Returns:
a org.w3c.dom.Element


<a href="https://github.com/autoplot/dev/search?q=singletonChildElement&unscoped_q=singletonChildElement">[search for examples]</a>

***
<a name="subArray"></a>
# subArray
subArray( int[] qube, int off, int len ) &rarr; int



### Parameters:
qube - a int[]
<br>off - a int
<br>len - a int

### Returns:
int[]


<a href="https://github.com/autoplot/dev/search?q=subArray&unscoped_q=subArray">[search for examples]</a>

