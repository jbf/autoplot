# org.das2.qstream.AsciiIntegerTransferTypeoptimized for sending integers
AsciiIntegerTransferType( int sizeBytes )


***
<a name="write"></a>
# write
write( double d, java.nio.ByteBuffer buffer ) &rarr; void



### Parameters:
d - a double
<br>buffer - a ByteBuffer

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=write&unscoped_q=write">[search for examples]</a>

