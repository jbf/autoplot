# org.das2.qstream.StreamCommentDescriptorFactory
StreamCommentDescriptorFactory( )


***
<a name="create"></a>
# create
create( org.w3c.dom.Element element ) &rarr; Descriptor



### Parameters:
element - a Element

### Returns:
org.das2.qstream.Descriptor


<a href="https://github.com/autoplot/dev/search?q=create&unscoped_q=create">[search for examples]</a>

