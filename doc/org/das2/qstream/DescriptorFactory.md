# org.das2.qstream.DescriptorFactoryFactor for converting the XML packet into the Descriptor that implements.
***
<a name="create"></a>
# create
create( org.w3c.dom.Element element ) &rarr; Descriptor

create the Descriptor from the XML element.

### Parameters:
element - a Element

### Returns:
the Descriptor.

<a href="https://github.com/autoplot/dev/search?q=create&unscoped_q=create">[search for examples]</a>

