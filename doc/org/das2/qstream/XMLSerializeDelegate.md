# org.das2.qstream.XMLSerializeDelegateExtension to SerializeDelegate that allows for embedding object within XML.
***
<a name="xmlFormat"></a>
# xmlFormat
xmlFormat( org.w3c.dom.Document doc, Object o ) &rarr; Element



### Parameters:
doc - a Document
<br>o - a Object

### Returns:
org.w3c.dom.Element


<a href="https://github.com/autoplot/dev/search?q=xmlFormat&unscoped_q=xmlFormat">[search for examples]</a>

***
<a name="xmlParse"></a>
# xmlParse
xmlParse( org.w3c.dom.Element e ) &rarr; Object



### Parameters:
e - a Element

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=xmlParse&unscoped_q=xmlParse">[search for examples]</a>

