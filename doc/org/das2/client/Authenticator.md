# org.das2.client.Authenticator
Authenticator( org.das2.client.DasServer dasServer )


Authenticator( org.das2.client.DasServer dasServer, String restrictedResourceLabel )


***
<a name="authenticate"></a>
# authenticate
authenticate(  ) &rarr; Key



### Returns:
org.das2.client.Key


<a href="https://github.com/autoplot/dev/search?q=authenticate&unscoped_q=authenticate">[search for examples]</a>

