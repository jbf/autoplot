# org.das2.components.BatchMaster.Timer
Timer( )


***
<a name="reportTime"></a>
# reportTime
reportTime( String msg ) &rarr; void



### Parameters:
msg - a String

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=reportTime&unscoped_q=reportTime">[search for examples]</a>

