# org.das2.components.FavoritesSelector.FavoritesListener
***
<a name="addFavoriteSelected"></a>
# addFavoriteSelected
addFavoriteSelected(  ) &rarr; Object



### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=addFavoriteSelected&unscoped_q=addFavoriteSelected">[search for examples]</a>

***
<a name="itemSelected"></a>
# itemSelected
itemSelected( Object o ) &rarr; void



### Parameters:
o - a Object

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=itemSelected&unscoped_q=itemSelected">[search for examples]</a>

