# org.das2.components.treetable.TreeTableCellRenderer
TreeTableCellRenderer( javax.swing.tree.TreeModel model )


***
<a name="getTableCellRendererComponent"></a>
# getTableCellRendererComponent
getTableCellRendererComponent( javax.swing.JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column ) &rarr; Component



### Parameters:
table - a JTable
<br>value - a Object
<br>isSelected - a boolean
<br>hasFocus - a boolean
<br>row - a int
<br>column - a int

### Returns:
java.awt.Component


<a href="https://github.com/autoplot/dev/search?q=getTableCellRendererComponent&unscoped_q=getTableCellRendererComponent">[search for examples]</a>

***
<a name="paint"></a>
# paint
paint( java.awt.Graphics g ) &rarr; void



### Parameters:
g - a Graphics

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=paint&unscoped_q=paint">[search for examples]</a>

***
<a name="setBounds"></a>
# setBounds
setBounds( int x, int y, int w, int h ) &rarr; void



### Parameters:
x - a int
<br>y - a int
<br>w - a int
<br>h - a int

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setBounds&unscoped_q=setBounds">[search for examples]</a>

