# org.das2.components.treetable.TreeTableNode
***
<a name="getColumnClass"></a>
# getColumnClass
getColumnClass( int columnIndex ) &rarr; Class



### Parameters:
columnIndex - a int

### Returns:
java.lang.Class


<a href="https://github.com/autoplot/dev/search?q=getColumnClass&unscoped_q=getColumnClass">[search for examples]</a>

***
<a name="getColumnCount"></a>
# getColumnCount
getColumnCount(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=getColumnCount&unscoped_q=getColumnCount">[search for examples]</a>

***
<a name="getColumnName"></a>
# getColumnName
getColumnName( int columnIndex ) &rarr; String



### Parameters:
columnIndex - a int

### Returns:
java.lang.String


<a href="https://github.com/autoplot/dev/search?q=getColumnName&unscoped_q=getColumnName">[search for examples]</a>

***
<a name="getValueAt"></a>
# getValueAt
getValueAt( int columnIndex ) &rarr; Object



### Parameters:
columnIndex - a int

### Returns:
java.lang.Object


<a href="https://github.com/autoplot/dev/search?q=getValueAt&unscoped_q=getValueAt">[search for examples]</a>

***
<a name="isCellEditable"></a>
# isCellEditable
isCellEditable( int columnIndex ) &rarr; boolean



### Parameters:
columnIndex - a int

### Returns:
boolean


<a href="https://github.com/autoplot/dev/search?q=isCellEditable&unscoped_q=isCellEditable">[search for examples]</a>

***
<a name="setValueAt"></a>
# setValueAt
setValueAt( Object value, int columnIndex ) &rarr; void



### Parameters:
value - a Object
<br>columnIndex - a int

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setValueAt&unscoped_q=setValueAt">[search for examples]</a>

