# org.das2.components.DasProgressWheelSmall 16x16 pixel progress wheel, designed intentionally for loading TCAs with X axis.
DasProgressWheel( )


***
<a name="cancel"></a>
# cancel
cancel(  ) &rarr; void



### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=cancel&unscoped_q=cancel">[search for examples]</a>

***
<a name="finished"></a>
# finished
finished(  ) &rarr; void



### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=finished&unscoped_q=finished">[search for examples]</a>

***
<a name="getPanel"></a>
# getPanel
getPanel( javax.swing.JComponent parent ) &rarr; JComponent

return the small component that visually shows the wheel.

### Parameters:
parent - a JComponent

### Returns:
a javax.swing.JComponent


<a href="https://github.com/autoplot/dev/search?q=getPanel&unscoped_q=getPanel">[search for examples]</a>

