# org.das2.components.ColorBarComponentColorBarComponent wraps a DasColorBar and DasCanvas to make a component.
ColorBarComponent( Datum min, Datum max, boolean isLog )
create a new ColorBarComponent

***
<a name="getColorBar"></a>
# getColorBar
getColorBar(  ) &rarr; DasColorBar

get the colorbar

### Returns:
the colorbar

<a href="https://github.com/autoplot/dev/search?q=getColorBar&unscoped_q=getColorBar">[search for examples]</a>

