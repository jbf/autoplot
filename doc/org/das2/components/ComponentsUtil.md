# org.das2.components.ComponentsUtilUtilities for managing components.
ComponentsUtil( )


***
<a name="createPopupCanvas"></a>
# createPopupCanvas
createPopupCanvas( java.awt.Component parent, String title, int width, int height ) &rarr; DasCanvas



### Parameters:
parent - a Component
<br>title - a String
<br>width - a int
<br>height - a int

### Returns:
org.das2.graph.DasCanvas


<a href="https://github.com/autoplot/dev/search?q=createPopupCanvas&unscoped_q=createPopupCanvas">[search for examples]</a>

***
<a name="getPdfButtonAction"></a>
# getPdfButtonAction
getPdfButtonAction( org.das2.graph.DasCanvas canvas ) &rarr; Action



### Parameters:
canvas - a DasCanvas

### Returns:
javax.swing.Action


<a href="https://github.com/autoplot/dev/search?q=getPdfButtonAction&unscoped_q=getPdfButtonAction">[search for examples]</a>

***
<a name="verifyVisible"></a>
# verifyVisible
verifyVisible( java.awt.Rectangle r ) &rarr; Rectangle

return null if the rectangle is visible, or a new rectangle where it
 should be moved if it is not.

### Parameters:
r - a Rectangle

### Returns:
null or a new suggested rectangle.

<a href="https://github.com/autoplot/dev/search?q=verifyVisible&unscoped_q=verifyVisible">[search for examples]</a>

