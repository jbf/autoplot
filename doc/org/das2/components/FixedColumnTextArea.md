# org.das2.components.FixedColumnTextArea
FixedColumnTextArea( )
Creates a new instance of FixedColumnTextPane

***
<a name="columnAt"></a>
# columnAt
columnAt( int pixelX ) &rarr; int



### Parameters:
pixelX - a int

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=columnAt&unscoped_q=columnAt">[search for examples]</a>

***
<a name="setColumnDivider"></a>
# setColumnDivider
setColumnDivider( int col ) &rarr; void



### Parameters:
col - a int

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setColumnDivider&unscoped_q=setColumnDivider">[search for examples]</a>

***
<a name="setColumnDividers"></a>
# setColumnDividers
setColumnDividers( int[] col ) &rarr; void



### Parameters:
col - a int[]

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=setColumnDividers&unscoped_q=setColumnDividers">[search for examples]</a>

