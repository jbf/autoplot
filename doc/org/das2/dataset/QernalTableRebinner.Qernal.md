# org.das2.dataset.QernalTableRebinner.Qernal
***
<a name="apply"></a>
# apply
apply( int x, int y, double value, double weight, double[][] s, double[][] w ) &rarr; void



### Parameters:
x - a int
<br>y - a int
<br>value - a double
<br>weight - a double
<br>s - a double[][]
<br>w - a double[][]

### Returns:
void (returns nothing)


<a href="https://github.com/autoplot/dev/search?q=apply&unscoped_q=apply">[search for examples]</a>

