# org.das2.dataset.ClippedTableDataSet
ClippedTableDataSet( QDataSet source, Datum xmin, Datum xmax, Datum ymin, Datum ymax )


ClippedTableDataSet( QDataSet source, DatumRange xrange, DatumRange yrange )


ClippedTableDataSet( QDataSet source, int xoffset, int xlength, int yoffset, int ylength )


***
<a name="length"></a>
# length
length(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=length&unscoped_q=length">[search for examples]</a>

length( int i ) &rarr; int<br>
***
<a name="rank"></a>
# rank
rank(  ) &rarr; int



### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=rank&unscoped_q=rank">[search for examples]</a>

***
<a name="value"></a>
# value
value( int i, int j ) &rarr; double



### Parameters:
i - a int
<br>j - a int

### Returns:
double


<a href="https://github.com/autoplot/dev/search?q=value&unscoped_q=value">[search for examples]</a>

