<a name="field: OK_OPTION"></a>
<a name="field: CANCEL_OPTION"></a>
<a name="getFont()"></a>
***
<a name="setExampleText(java.lang.String)"></a>
# setExampleText
setExampleText( String text ) &rarr; void



### Parameters:

### Returns:
void


<a href="https://github.com/autoplot/dev/search?q=setExampleText&unscoped_q=setExampleText">search for examples</a>

<a name="setFont(Font)"></a>
***
<a name="setFontCheck(JFontChooser.FontCheck)"></a>
# setFontCheck
setFontCheck( ZoeloeSoft.projects.JFontChooser.JFontChooser.FontCheck c ) &rarr; void

allows an arbitrary string to be indicated for any font.  For example,
 in Autoplot, we look to see if the font can be embedded.

### Parameters:
c - 

### Returns:
void


<a href="https://github.com/autoplot/dev/search?q=setFontCheck&unscoped_q=setFontCheck">search for examples</a>

***
<a name="showDialog(Font)"></a>
# showDialog
showDialog( java.awt.Font font ) &rarr; int



### Parameters:

### Returns:
int


<a href="https://github.com/autoplot/dev/search?q=showDialog&unscoped_q=showDialog">search for examples</a>

<a name="showDialog()"></a>
